CFLAGS= -Wall -g

all: index 

index: libsl index.o
	gcc $(CFLAGS) index.o -o index -L. -lsl

index.o: index.c index.h
	gcc $(CFLAGS) -c index.c

libsl: sorted-list.o 
	ar rcs libsl.a sorted-list.o

sorted-list.o: sorted-list.c sorted-list.h
	gcc $(CFLAGS) -c sorted-list.c

clean:
	rm *.o libsl.a index
