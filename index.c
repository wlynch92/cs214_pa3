#include "index.h"

/*
 * The following are comparator functions for use in sorted-list
 * CompareFuncT functions
 */
int compareInts(void *p1, void *p2) {
    int i1 = *(int*)p1;
    int i2 = *(int*)p2;
    return i1-i2;
}

int compareNodeCount(const void *p1,const void *p2){
    int i1 = (*(SLNode **)p1)->count;
    int i2 = (*(SLNode **)p2)->count;
    return i2-i1;
}

int compareStrings(void *p1, void *p2) {
    char *s1 = p1;
    char *s2 = p2;
    return strcmp(s1, s2);
}

/*
 * Creates and returns an indexer struct
 */
Index* indexerCreate() {
    
    Index *indexer = (Index*)malloc(sizeof(Index));
    indexer->tokenlist = SLCreate(compareStrings, 0, 1);
    indexer->wordcount = 0;
    return indexer;
}

/*
 * Cleanly destroys the indexer and its subcomponents
 */
void indexerDestroy(Index *indexer) {

    SLNode *tmp = indexer->tokenlist->head;
    if (indexer != NULL) {
        for (; tmp != NULL; tmp = tmp->next) {
            SLDestroy(tmp->fileSublist); /* free sublist in each token node */
        }
        SLDestroy(indexer->tokenlist); /* free each token node */
    }
    free(indexer);
}

/* 
 * Inserts a given word into an indexer. 
 */
void insert(Index *indexer, char* lowercase, char* filename) {
   
    SLNode *token, *file;
    
    char *tmplowercase=(char *)malloc(sizeof(char)*(strlen(lowercase)+1));
    strcpy(tmplowercase,lowercase);
    
    token = SLInsert(indexer->tokenlist, (void *)tmplowercase);
    
    if (token != NULL) {
        if (token->fileSublist == NULL) { /* check if token already has its file list */
            token->fileSublist = SLCreate(compareStrings, 1, 0); /* if not, make one */
        }
        char *tmpfilename=(char *)calloc(strlen(filename)+1,sizeof(char)*(strlen(filename)+1));
        strcpy(tmpfilename,filename);
        file = SLInsert(token->fileSublist, (void *)tmpfilename);
    }

}

/* This function determines whether or not a given file is a directory
 * or file, and calls the corresponding index function */
void indexStart(char* input, Index *indexer){
    struct stat s;
    stat(input, &s);
    if (S_ISDIR(s.st_mode)){
        DIR* dir = opendir(input);
        if(dir != NULL){
            indexDir(dir,input, indexer);
            closedir(dir);
        }
    } else {
        FILE* file = fopen(input,"r");
        if (file != NULL){
            indexFile(file, input, indexer);
            fclose(file);
        }
    }
}

/* This function takes in a file pointer and parses through and indexes
 * every word via a sorted-list.
 */
void indexFile(FILE* file, char* filename, Index *indexer){
    int index = 0;
    char c = fgetc(file); /* current character being read in the file */
    char *buffer = (char*)calloc(1024,sizeof(char)*1024);
    
    while (c != EOF) {
        if (isalnum(c) != 0) { /* the current char is alphanumeric */
            if (c >= 65 && c <= 90){ 
                c+= 32; /* ascii manipulation */
            }
            buffer[index] = c;
            if (index >= 1023) {
                fprintf(stderr, "not enough space for more characters, ending program.\n");
                exit(1);
            }
            index++;
        } else { /* token found, normalizing it to lowercase */
            buffer[index] = '\0';
            if (index != 0) {
                insert(indexer, buffer, filename); /* insert into tokenlist */
            }
            index = 0;
        }
        c = fgetc(file);
    }
    free(buffer);
}

/* Opens given directory and makes a call back to indexStart() for 
 * each item in the directory to determine whether it is a dir or file.
 */
void indexDir(DIR* dir,char* parent, Index *indexer){
    struct dirent* de;
    char* filename;
    int offset=1;
    if (parent[strlen(parent)-1]!='/'){
        offset=2;
    }
    de = readdir(dir);
    while (de != NULL){
        if (strcmp(de->d_name,".")!=0 && strcmp(de->d_name,"..")!=0){
            filename=(char *)malloc(sizeof(char)*(strlen(parent)+strlen(de->d_name)+offset));
            strcpy(filename,parent);
            if (offset==2){
                strcat(filename,"/");
            }
            strcat(filename,de->d_name);
            strcat(filename,"\0");
            indexStart(filename, indexer);
            free(filename);
        }
        de = readdir(dir);
    }

}

/* 
 * Writes given indexer to filename. If file already exists, prompts user if they 
 * wish to overwrite the file before write occurs.
 */
void writeIndex(Index *indexer, char* filename) {
    FILE* fp = fopen(filename,"r");
    if (fp != NULL){
        char str[128]="a";
        while (strcmp(str,"y")!=0 && strcmp(str,"n")!=0){
            printf("WARNING: %s already exists! Overwrite? (y/n)\n",filename);
            scanf("%s",str);
        }
        if (strcmp(str,"n")==0){
            fclose(fp);
            return;
        }
        fclose(fp); 
    }
    fp= fopen(filename,"w");
    if (fp != NULL){
        SortedListIteratorPtr iter=SLCreateIterator(indexer->tokenlist);
        SLNode *tokenNode=SLNextItem(iter);
        while (tokenNode != NULL){
           
            fprintf(fp,"<list> %s\n",(char*)tokenNode->val);
            int i=0,size=tokenNode->fileSublist->count;
            SLNode* nodeList[size];
            for (i=0; i<size; i++){
                nodeList[i]=NULL;
            }
            i=0;
            SortedListIteratorPtr subIter=SLCreateIterator(tokenNode->fileSublist);

            SLNode *fileNode=SLNextItem(subIter);
            while (fileNode != NULL){
                nodeList[i]=fileNode;
                i++;
                fileNode=SLNextItem(subIter);
            }
            SLDestroyIterator(subIter);
            qsort(nodeList,size,sizeof(SLNode*),compareNodeCount);
            i=0;
            while (i<5 && i<size){
                fprintf(fp,"%s %d ",(char *)(nodeList[i]->val),nodeList[i]->count);
                i++;
            }
            tokenNode=SLNextItem(iter);
            
            fprintf(fp,"\n</list>\n");
        }
        SLDestroyIterator(iter);
    }
    fclose(fp);
}

/* 
 * Program takes in 2 variables:
 * 1) File to output result to
 * 2) Input file or directory to index
 *
 * If incorrect number of arguements are given, error is thrown
 */
int main(int argc, char* argv[]){
    if (argc!=3){
        fprintf(stderr,"Incorrect arguements given\n");
        return 1;
    }

    Index *indexer = indexerCreate();
    indexStart(argv[2], indexer);
    writeIndex(indexer,argv[1]);
    indexerDestroy(indexer);

    return 0;
}
