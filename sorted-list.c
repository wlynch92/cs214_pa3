/* 
 * sorted-list.c
 * Written by
 *              Bill Lynch  <wlynch92@eden.rutgers.edu>
 *              Jenny Shi   <jenny.shi@rutgers.edu>
 *
 * Runtime: Worst case O(n^2)
 * This occurs when we insert to the end of the list everytime we call SLInsert.
 */

#include "sorted-list.h"

/*
 * When your sorted list is used to store objects of some type, since the
 * type is opaque to you, you will need a comparator function to order
 * the objects in your sorted list.
 *
 * You can expect a comparator function to return -1 if the 1st object is
 * smaller, 0 if the two objects are equal, and 1 if the 2nd object is
 * smaller.
 *
 * Note that you are not expected to implement any comparator functions.
 * You will be given a comparator function when a new sorted list is
 * created.
 */


/*
 * SLCreate creates a new, empty sorted list.  The caller must provide
 * a comparator function that can be used to order objects that will be
 * kept in the list.
 * 
 * If the function succeeds, it returns a (non-NULL) SortedListT object.
 * Else, it returns NULL.
 *
 * valsort: if 0, sort by value, else sort by count
 * a2z: if 0, sort by descending order, else ascending
 *
 * You need to fill in this function as part of your implementation.
 */
SortedListPtr SLCreate(CompareFuncT cf, int valsort, int a2z) {

    if (cf != NULL) { /* if cf is NULL, list will not be created */
        /* initializing a new list */
        SortedListPtr list = (SortedListPtr)malloc(sizeof(struct SortedList));
        list->head = NULL; 
        list->count=0;
        list->sortval = valsort;
        if (a2z){ /* ascending order */
            list->ascend=-1;
        } else { /* decending order */
            list->ascend=1;
        }
        list->CompareFuncT = cf; /* set cf to the CompareFuncT pointer */
        return list;
    }
    return NULL;
}

/*
 * SLDestroy destroys a list, freeing all dynamically allocated memory.
 * Ojects should NOT be deallocated, however.  That is the responsibility
 * of the user of the list.
 *
 * You need to fill in this function as part of your implementation.
 */
void SLDestroy(SortedListPtr list) {

    SLNode *tmp = list->head;
    SLNode *freetmp;
    while (tmp != NULL) {
        tmp->ptrcount--;
        if (tmp->next == NULL) {
            /* Only remove if no iterator is pointing to it */
            if (tmp->ptrcount <=0){
                free(tmp->val);
                free(tmp);
            }
            break;
        }
        else {
            freetmp = tmp;
            tmp = freetmp->next;
            /* Only remove if no iterator is pointing to it */
            if (freetmp->ptrcount <=0){
                free(freetmp->val);
                free(freetmp);
            }
        }    
    }
    free(list);
}


/*
 * SLInsert inserts a given object into a sorted list, maintaining sorted
 * order of all objects in the list.  If the new object is equal to a subset
 * of existing objects in the list, then the subset can be kept in any
 * order.
 *
 * If the function succeeds, it returns the inserted node.  Else, it returns NULL.
 *
 * You need to fill in this function as part of your implementation.
 */
SLNode* SLInsert(SortedListPtr list, void *newObj) {

    if (list == NULL || newObj == NULL) return NULL; /* invalid arguments */
    /* create new node with the given newObj data */
  
    SLNode *node = (SLNode*)malloc(sizeof(SLNode));
    node->val = newObj;
    node->next = NULL;
    node->ptrcount = 0;
    node->count = 1;
    node->fileSublist = NULL;
    int res; 


    if (list->head == NULL) { /* linked list is empty */
        list->head = node; /* node is the first in the list */
        node->ptrcount++;
        list->count++;
        return node;
    } 
    else { /* must traverse through list and find the right spot for insertion */
        SLNode *ptr;
        SLNode *prev;

        for (ptr = list->head, prev= NULL; ptr != NULL; prev = ptr, ptr = ptr->next) { 
            res=(list->CompareFuncT(ptr->val, node->val))*list->ascend;

            if (res == 0) {
                ptr->count++;
                free(node->val);
                free(node);
                return ptr; /* if two values are equal, no insertion is done */
            }
            else if (res < 0) { /* if current node is less than given node, insert before current node */
                if (prev == NULL) { /* given node is placed at the beginning of the list */
                    node->next = ptr;
                    list->head = node; /* given node is now the new head of the list */
                } 
                else {
                    prev->next = node;
                    node->next = ptr;
                }
                node->ptrcount++; /* update ptrcount for given node */
                list->count++;
                return node;
            }
            /* Insert at end of list */
            if (ptr->next == NULL) {
                ptr->next = node;
                node->next = NULL;
                node->ptrcount++;
                list->count++;
                return node;
            }

        }
        return NULL; 
    }    
}


/*
 * SLRemove removes a given object from a sorted list.  Sorted ordering
 * should be maintained.
 *
 * If the function succeeds, it returns 1.  Else, it returns 0.
 *
 * You need to fill in this function as part of your implementation.
 */
int SLRemove(SortedListPtr list, void *newObj) {

    if (list == NULL || newObj == NULL) return 0; /* nothing to remove */

    SLNode *ptr = list->head;
    SLNode *prev = NULL;
    for (; ptr != NULL; prev = ptr, ptr = ptr->next) {
        int res = list->CompareFuncT(ptr->val, newObj);
        if (res == 0) { /* match is found */
            if ((prev == NULL) && (ptr->next==NULL)) list->head=NULL;
            else if (prev == NULL) list->head = ptr->next; /* match is the first node */
            else if (ptr->next == NULL) prev->next = NULL; /* match is the last node */
            else prev->next = ptr->next;
            ptr->ptrcount--;
            /* Only free if no iterator is pointing to node */
            if (ptr->ptrcount<=0){
                free(ptr->val);
                free(ptr); /* free node */
            }
            return 1;       
        }
    }
    return 0;
}

/*
 * SLCreateIterator creates an iterator object that will allow the caller
 * to "walk" through the list from beginning to the end using SLNextItem.
 *
 * If the function succeeds, it returns a non-NULL SortedListIterT object.
 * Else, it returns NULL.
 *
 * You need to fill in this function as part of your implementation.
 */

SortedListIteratorPtr SLCreateIterator(SortedListPtr list){
    /* Only do something if the list is not null */
    if (list != NULL){
        SortedListIteratorPtr iter = (SortedListIteratorPtr)malloc(sizeof(struct SortedListIterator));
        /* Initialize the iterator to the head */
        iter->curr=list->head;
        if (iter->curr != NULL){
            /* Increase pointer count */
            iter->curr->ptrcount++;
        }
        return iter;
    }
    return NULL;
}


/*
 * SLDestroyIterator destroys an iterator object that was created using
 * SLCreateIterator().  Note that this function should destroy the
 * iterator but should NOT affect the original list used to create
 * the iterator in any way.
 *
 * You need to fill in this function as part of your implementation.
 */

void SLDestroyIterator(SortedListIteratorPtr iter){
    if (iter->curr != NULL){
        /* Decrease pointer count, check if this triggers any items to be freed.
         * Free only happens iff only the iterator is the only thing pointing to the node.
         * i.e. if the node was removed from the list, but an iterator was on that node.
         */
        iter->curr->ptrcount--;
        /* Only free if nothing else is pointing to the node */
        if (iter->curr->ptrcount <= 0){
            if (iter->curr->next != NULL){
                iter->curr->next->ptrcount--;
            }
            free(iter->curr->val);
            free(iter->curr);
        }
    }
    free(iter);
}


/*
 * SLNextItem returns the next object in the list encapsulated by the
 * given iterator.  It should return a NULL when the end of the list
 * has been reached.
 *
 * One complication you MUST consider/address is what happens if a
 * sorted list encapsulated within an iterator is modified while that
 * iterator is active.  For example, what if an iterator is "pointing"
 * to some object in the list as the next one to be returned but that
 * object is removed from the list using SLRemove() before SLNextItem()
 * is called.
 *
 * You need to fill in this function as part of your implementation.
 */

SLNode *SLNextItem(SortedListIteratorPtr iter){
    if (iter->curr != NULL){
        SLNode *retval=iter->curr;
        iter->curr->ptrcount--;
        SLNode *tmp = iter->curr->next;

        /* If this node is supposed to be removed, remove and return NULL */
        if (iter->curr->ptrcount <= 0){
            free(iter->curr->val);
            free(iter->curr);
            iter->curr=NULL;
            return NULL;
        }
        if (tmp != NULL){
            tmp->ptrcount++;
        }
        iter->curr=tmp;
        /* Only return a value iff we are not at the end of the list,
         * i.e. the current pointer is not null
         */
        return retval;
    }
    return NULL;
}
